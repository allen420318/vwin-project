const proxyReq = (domain) => {
  return (proxyRes, req, res) => {
    const cookies = proxyRes.headers['set-cookie']
    if (cookies) {
      let reqHost = req.headers.host || 'localhost'
      if (reqHost && reqHost.includes(':') >= 0) {
        reqHost = reqHost.split(':')[0]
      }
      const newCookies = cookies.map((cookie) =>
        cookie.replace(domain, reqHost)
      )
      delete proxyRes.headers['set-cookie']
      proxyRes.headers['set-cookie'] = newCookies
    }
  }
}

export const proxyConfig = {
  '/api': {
    target: 'http://rich-web.w9fun.com/',
    pathRewrite: {
      '^/api': '/',
    },
    changeOrigin: true,
    headers: { 'X-Forwarded-For': '' },
    onProxyRes: proxyReq('.w9fun.com'),
  },
}

export const headConfig = {
  title: process.env.npm_package_name || '',
  meta: [
    { charset: 'utf-8' },
    { name: 'viewport', content: 'width=device-width, initial-scale=1' },
    {
      hid: 'description',
      name: 'description',
      content: process.env.npm_package_description || '',
    },
  ],
  link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
}

export default {
  proxyConfig,
  headConfig,
}
