import { proxyConfig, headConfig } from './common.js'

export default {
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  mode: 'universal',
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: 'server',
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: headConfig,
  /*
   ** Global CSS
   */
  css: [
    'element-ui/lib/theme-chalk/index.css',
    'vue-slick-carousel/dist/vue-slick-carousel.css',
    { src: '~/assets/style//mb/common.scss', lang: 'scss' },
  ],
  styleResources: {
    scss: ['~/assets/style/mb/common.scss'],
  },
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [
    { src: '@/plugins/element-ui', ssr: true },
    '~/plugins/common',
    '~/plugins/vee-validate',
    '~/plugins/vue-slick-carousel.js',
    '~/plugins/vue-awesome-swiper.js',
    '~/plugins/route',
  ],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: {
    dirs: [
      '~/components',
      {
        path: '~/components/mb/',
      },
    ],
  },
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    '@nuxtjs/pwa',
    '@nuxtjs/style-resources',
    [
      'nuxt-vuex-localstorage',
      {
        localStorage: ['user', 'login'],
      },
    ],
    [
      'nuxt-i18n',
      {
        locales: [
          {
            name: '繁體',
            code: 'zh-Hant',
            iso: 'zh-TW',
            file: 'zh-TW.js',
          },
          {
            name: 'English',
            code: 'en',
            iso: 'en-US',
            file: 'en.js',
          },
        ],
        lazy: true,
        langDir: 'i18n/',
        defaultLocale: 'zh-Hant',
        strategy: 'no_prefix',
        detectBrowserLanguage: {
          useCookie: true,
          cookieKey: 'lang',
          alwaysRedirect: false,
        },
      },
    ],
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    proxy: true,
  },
  proxy: proxyConfig,
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {
    transpile: [/^element-ui/, 'vee-validate/dist/rules'],
  },
  buildDir: '.nuxt-mb',
  env: {
    baseUrl: process.env.API_URL || '/api/',
  },
  server: {
    port: process.env.MOBILE_PORT || 3000,
  },
  dir: {
    layouts: 'layouts/mb',
    pages: 'pages/mb',
    store: 'store/mb',
  },
}
