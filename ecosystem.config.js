module.exports = {
  apps: [
    {
      name: 'richbet-nuxt-mb', // App name that shows in `pm2 ls`
      exec_mode: 'cluster', // enables clustering
      instances: 1, // default instance is 1
      script: './node_modules/nuxt/bin/nuxt.js',
      args: 'start --config-file config/nuxt.config-mb.js',
    },
    {
      name: 'richbet-nuxt-pc', // App name that shows in `pm2 ls`
      exec_mode: 'cluster', // enables clustering
      instances: 1, // default instance is 1
      script: './node_modules/nuxt/bin/nuxt.js',
      args: 'start --config-file config/nuxt.config-pc.js',
    },
  ],
}
