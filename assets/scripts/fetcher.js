import axios from 'axios'
axios.defaults.baseURL = process.env.baseUrl
axios.defaults.headers.common['Content-Type'] = 'application/json'
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
axios.defaults.withCredentials = true

export const fetcher = async (APIUrl, httpVerb = 'POST', payload) => {
  const lang = window.$nuxt.$store.$i18n.loadedLanguages[0]
  axios.defaults.headers.common['X-Requested-Lang'] = lang
  const axiosConfig = {
    method: httpVerb.toLowerCase(),
    url: APIUrl,
    data: payload,
  }

  return await axios(axiosConfig)
    .then(function (response) {
      return response.data
    })
    .catch(function (error) {
      const errRes = error.response
      return Promise.reject(errRes.data)
    })
}
