export default {
  userAccount: /^[a-zA-Z]{1}[0-9a-zA-Z]{4,15}$/,
  userPassword: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,16}$/,
  phoneNumber: /^[0-9]*$/,
  email: /^\w+((-\w+)|(\.\w+))*@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/,
  accountEnName: /^[a-zA-Z]*$/,
  bankAccount: /^[0-9]*$/,
}
