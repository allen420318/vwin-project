const store = {
  state: () => ({
    loginInfo: {},
  }),
  mutations: {
    setObject(state, payload) {
      state[payload.item] = payload.data
    },
    removeObject(state, payload) {
      state[payload.item] = {}
    },
  },
  actions: {
    setLoginInfo({ commit }, val) {
      commit('setObject', { item: 'loginInfo', data: val })
    },
    clearloginInfo({ commit }) {
      commit('removeObject', { item: 'loginInfo' })
    },
  },
  getters: {},
}

export default store
