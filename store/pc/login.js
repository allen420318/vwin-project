const store = {
  state: () => ({
    isLogin: false,
    memberInfo: {},
    vipInfo: {},
    userCredits: 0,
  }),
  mutations: {
    setObject(state, Payload) {
      state[Payload.item] = Payload.data
    },
    removeObject(state, Payload) {
      state[Payload.item] = {}
    },
    resetBoolen(state, Payload) {
      state[Payload.item] = false
    },
    resetValue(state, Payload) {
      state[Payload.item] = 0
    },
  },
  actions: {
    setLoginStatus({ commit }, val) {
      commit('setObject', { item: 'isLogin', data: val })
    },
    setMemberInfo({ commit }, val) {
      commit('setObject', { item: 'memberInfo', data: val })
    },
    setVipInfo({ commit }, val) {
      commit('setObject', { item: 'vipInfo', data: val })
    },
    setUserCredits({ commit }, val) {
      commit('setObject', { item: 'userCredits', data: val })
    },
    removeLoginStatus({ commit }) {
      commit('resetBoolen', { item: 'isLogin' })
    },
    removetMemberInfo({ commit }) {
      commit('removeObject', { item: 'memberInfo' })
    },
    removetVipInfo({ commit }) {
      commit('removeObject', { item: 'vipInfo' })
    },
    removetUserCredits({ commit }) {
      commit('resetValue', { item: 'userCredits' })
    },
  },
  getters: {
    getIsLogin: (state) => {
      return state.isLogin
    },
  },
}

export default store
