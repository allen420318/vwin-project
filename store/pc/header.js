const store = {
  state: () => ({
    headerStatus: true,
    footerStatus: true,
    sideMenuStatus: true,
  }),
  mutations: {
    setObject(state, payload) {
      state[payload.item] = payload.data
    },
    resetObject(state, payload) {
      state[payload.item] = true
    },
  },
  actions: {
    setHeaderStatus({ commit }, val) {
      commit('setObject', { item: 'headerStatus', data: val })
    },
    setFooterStatus({ commit }, val) {
      commit('setObject', { item: 'footerStatus', data: val })
    },
    setSideMenuStatus({ commit }, val) {
      commit('setObject', { item: 'sideMenuStatus', data: val })
    },
    clearHeaderStatus({ commit }) {
      commit('resetObject', { item: 'headerStatus' })
    },
    clearFooterStatus({ commit }) {
      commit('resetObject', { item: 'footerStatus' })
    },
    clearSideMenuStatus({ commit }) {
      commit('resetObject', { item: 'sideMenuStatus' })
    },
  },
  getters: {
    getHeaderStatus: (state) => {
      return state.headerStatus
    },
    getFooterStatus: (state) => {
      return state.footerStatus
    },
    getSideMenuStatus: (state) => {
      return state.sideMenuStatus
    },
  },
}

export default store
