const store = {
  state: () => ({
    depositMode: '',
    depositAmount: '',
    depositData: {},
  }),
  mutations: {
    setObject(state, payload) {
      state[payload.item] = payload.data
    },
    removeObject(state, payload) {
      state[payload.item] = ''
    },
  },
  actions: {
    setDepositMode({ commit }, val) {
      commit('setObject', { item: 'depositMode', data: val })
    },
    setDepositData({ commit }, val) {
      commit('setObject', { item: 'depositData', data: val })
    },
    setDepositAmount({ commit }, val) {
      commit('setObject', { item: 'depositAmount', data: val })
    },
    clearDepositMode({ commit }) {
      commit('removeObject', { item: 'depositMode' })
    },
    clearDepositData({ commit }) {
      commit('removeObject', { item: 'depositData' })
    },
    clearDepositAmount({ commit }) {
      commit('removeObject', { item: 'depositAmount' })
    },
  },
  getters: {},
}

export default store
