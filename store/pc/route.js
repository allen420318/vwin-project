const store = {
  state: () => ({
    currentPath: '',
  }),
  mutations: {
    setObject(state, payload) {
      state[payload.item] = payload.data
    },
    resetObject(state, payload) {
      state[payload.item] = ''
    },
  },
  actions: {
    setCurrentPath({ commit }, val) {
      commit('setObject', { item: 'currentPath', data: val })
    },
    clearCurrentPath({ commit }) {
      commit('resetObject', { item: 'currentPath' })
    },
  },
  getters: {},
}

export default store
