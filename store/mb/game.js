const store = {
  state: () => ({
    selectGameProvider: {
      id: '',
      number: 0,
    },
    selectGame: {},
  }),
  mutations: {
    setObject(state, payload) {
      state[payload.item] = payload.data
    },
    removeObject(state, payload) {
      state[payload.item] = {}
    },
    resetObject(state, payload) {
      state[payload.item] = {
        id: '',
        number: 0,
      }
    },
  },
  actions: {
    setSelectGameProvider({ commit }, val) {
      commit('setObject', { item: 'selectGameProvider', data: val })
    },
    setSelectGame({ commit }, val) {
      commit('setObject', { item: 'selectGame', data: val })
    },
    clearSelectGameProvider({ commit }) {
      commit('resetObject', { item: 'selectGameProvider' })
    },
    clearSelectGame({ commit }) {
      commit('removeObject', { item: 'selectGame' })
    },
  },
  getters: {},
}

export default store
