const store = {
  state: () => ({
    transactionType: {},
    transactionId: '',
    betStep: 1,
  }),
  mutations: {
    setObject(state, payload) {
      state[payload.item] = payload.data
    },
    removeObject(state, payload) {
      state[payload.item] = ''
    },
    resetNumber(state, payload) {
      state[payload.item] = 1
    },
  },
  actions: {
    setTransactionType({ commit }, val) {
      commit('setObject', { item: 'transactionType', data: val })
    },
    setTransactionId({ commit }, val) {
      commit('setObject', { item: 'transactionId', data: val })
    },
    setBetStep({ commit }, val) {
      commit('setObject', { item: 'betStep', data: val })
    },
    clearTransactionType({ commit }) {
      commit('removeObject', { item: 'TransactionType' })
    },
    clearTransactionId({ commit }) {
      commit('removeObject', { item: 'TransactionId' })
    },
    clearBetStep({ commit }) {
      commit('resetNumber', { item: 'betStep' })
    },
  },
  getters: {},
}

export default store
