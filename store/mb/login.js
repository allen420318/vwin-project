const store = {
  state: () => ({
    isLogin: false,
    memberInfo: {},
  }),
  mutations: {
    setObject(state, Payload) {
      state[Payload.item] = Payload.data
    },
    removeObject(state, Payload) {
      state[Payload.item] = {}
    },
    resetBoolen(state, Payload) {
      state[Payload.item] = false
    },
  },
  actions: {
    setLoginStatus({ commit }, val) {
      commit('setObject', { item: 'isLogin', data: val })
    },
    setMemberInfo({ commit }, val) {
      commit('setObject', { item: 'memberInfo', data: val })
    },
    removeLoginStatus({ commit }) {
      commit('resetBoolen', { item: 'isLogin' })
    },
    removetMemberInfo({ commit }) {
      commit('removeObject', { item: 'memberInfo' })
    },
  },
  getters: {
    getIsLogin: (state) => {
      return state.isLogin
    },
  },
}

export default store
