const store = {
  state: () => ({
    messageMode: '',
    messageData: {},
    messageOperateMask: false,
    messageEditMode: false,
    messageDeleteMask: false,
  }),
  mutations: {
    setObject(state, payload) {
      state[payload.item] = payload.data
    },
    removeObject(state, payload) {
      state[payload.item] = ''
    },
  },
  actions: {
    setMessageMode({ commit }, val) {
      commit('setObject', { item: 'messageMode', data: val })
    },
    setMessageData({ commit }, val) {
      commit('setObject', { item: 'messageData', data: val })
    },
    setMessageOperateMask({ commit }, val) {
      commit('setObject', { item: 'messageOperateMask', data: val })
    },
    setMessageEditMode({ commit }, val) {
      commit('setObject', { item: 'messageEditMode', data: val })
    },
    setMessageDeleteMask({ commit }, val) {
      commit('setObject', { item: 'messageDeleteMask', data: val })
    },
    clearMessageMode({ commit }) {
      commit('removeObject', { item: 'messageMode' })
    },
    clearMessageData({ commit }) {
      commit('removeObject', { item: 'messageData' })
    },
  },
  getters: {},
}

export default store
