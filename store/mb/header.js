const store = {
  state: () => ({
    headerStatus: -1,
    navbarStatus: -1,
    headerTitle: '',
    headerIcon: [],
    headerText: [],
    headerComplete: false,
  }),
  mutations: {
    setObject(state, payload) {
      state[payload.item] = payload.data
    },
    removeObject(state, payload) {
      state[payload.item] = []
    },
  },
  actions: {
    setHeaderStatus({ commit }, val) {
      commit('setObject', { item: 'headerStatus', data: val })
    },
    setNavbarStatus({ commit }, val) {
      commit('setObject', { item: 'navbarStatus', data: val })
    },
    setHeaderTitle({ commit }, val) {
      commit('setObject', { item: 'headerTitle', data: val })
    },
    setHeaderIcon({ commit }, val) {
      commit('setObject', { item: 'headerIcon', data: val })
    },
    setHeaderText({ commit }, val) {
      commit('setObject', { item: 'headerText', data: val })
    },
    setHeaderComplete({ commit }, val) {
      commit('setObject', { item: 'headerComplete', data: val })
    },
    clearHeaderIcon({ commit }) {
      commit('removeObject', { item: 'headerIcon' })
    },
    clearHeaderText({ commit }) {
      commit('removeObject', { item: 'headerText' })
    },
  },
  getters: {
    getHeaderStatus: (state) => {
      return state.headerStatus
    },
    getNavbarStatus: (state) => {
      return state.navbarStatus
    },
  },
}

export default store
