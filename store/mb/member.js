const store = {
  state: () => ({
    updateCategory: '',
    updateInfo: {
      realName: '',
      birthday: '',
      email: '',
      lineID: '',
      phoneNumber: '',
      countryCode: '',
      gender: '',
    },
    validate: '',
  }),
  mutations: {
    setObject(state, payload) {
      state[payload.item] = payload.data
    },
    setObjectProperty(state, payload) {
      state[payload.state][payload.property] = payload.data
    },
    removeObject(state, payload) {
      state[payload.item] = ''
    },
    removeObjectProperty(state, payload) {
      state[payload.state][payload.property] = ''
    },
  },
  actions: {
    setUpdateInfo({ commit }, payload) {
      commit('setObjectProperty', payload)
    },
    clearUpdateInfo({ commit }, payload) {
      commit('removeObjectProperty', payload)
    },
    setUpdateCategory({ commit }, val) {
      commit('setObject', { item: 'updateCategory', data: val })
    },
    clearUpdateCategory({ commit }) {
      commit('removeObject', { item: 'updateCategory' })
    },
    setValidate({ commit }, val) {
      commit('setObject', { item: 'validate', data: val })
    },
    clearValidate({ commit }) {
      commit('removeObject', { item: 'validate' })
    },
  },
  getters: {},
}

export default store
