import { fetcher } from '~/assets/scripts/fetcher.js'

export const getGameProvider = async (data) => {
  const result = await fetcher('game/provider', 'POST', {
    device: data.device,
    platform_type: data.gameType,
  })
  return result
}

export const getGameList = async (provider = '') => {
  const result = await fetcher(`game/list/${provider}`, 'GET')
  return result
}

export const openGame = async (
  gameProvider,
  gameType,
  gameId = '',
  demo = ''
) => {
  const result = await fetcher(
    `game/open/?provider=${gameProvider}&type=${gameType}&game_id=${gameId}&demo=${demo}`,
    'GET'
  )
  return result
}

export const getFavoriteList = async (provider) => {
  const result = await fetcher(`favorite/list/${provider}`, 'GET')
  return result
}

export const addFavorite = async (data) => {
  const result = await fetcher('favorite/add', 'POST', {
    provider: data.provider,
    game_type: data.gameType,
    game_no: data.gameNumber,
  })
  return result
}

export const removeFavorite = async (data) => {
  const result = await fetcher('favorite/remove', 'POST', {
    provider: data.provider,
    game_type: data.gameType,
    game_no: data.gameNumber,
  })
  return result
}
export default {
  getGameProvider,
  getGameList,
  openGame,
  getFavoriteList,
  addFavorite,
  removeFavorite,
}
