import { fetcher } from '~/assets/scripts/fetcher.js'

export const userLogout = async () => {
  const result = await fetcher('user/logout', 'GET')
  return result
}

export const checkLoginStatus = async () => {
  const result = await fetcher('user/check', 'GET')
  return result
}

export const getUserDetailInfo = async () => {
  const result = await fetcher('user/detail', 'GET')
  return result
}

export const updateUserDetailInfo = async (data) => {
  const result = await fetcher('user/detail/update', 'POST', {
    bank_account_name: data.realName,
    birthday: data.birthday,
    email: data.email,
    line_id: data.lineID,
    phone_no: data.phoneNumber,
    country_code: data.countryCode,
    gender: data.gender,
  })
  return result
}

export const getUserCredits = async () => {
  const result = await fetcher('user/credits', 'GET')
  return result
}

export const getVerificationPhone = async (data) => {
  const result = await fetcher('user/send-valid-code-phone', 'POST', {
    phone_no: data.phoneNumber,
    country_code: data.countryCode,
  })
  return result
}

export const getVerificationEmail = async (data) => {
  const result = await fetcher('user/send-valid-code-email', 'POST', {
    email: data.email,
  })
  return result
}

export const getUnreadNumber = async () => {
  const result = await fetcher('user/unreads', 'GET')
  return result
}

export const getMessageList = async (type) => {
  const result = await fetcher(`user/list/${type}`, 'GET')
  return result
}

export const getMessageDetail = async (id) => {
  const result = await fetcher(`user/detail/${id}`, 'GET')
  return result
}

export const readMessage = async (data) => {
  const result = await fetcher('user/read', 'POST', {
    ids: data.id,
  })
  return result
}

export const changePassword = async (data) => {
  const result = await fetcher('user/change-password', 'POST', {
    password_new: data.newPassword,
    password_old: data.oldPassword,
    target: data.type,
  })
  return result
}

export default {
  userLogout,
  checkLoginStatus,
  getUserDetailInfo,
  updateUserDetailInfo,
  getUserCredits,
  getVerificationPhone,
  getVerificationEmail,
  getUnreadNumber,
  getMessageList,
  getMessageDetail,
  readMessage,
  changePassword,
}
