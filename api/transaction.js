import { fetcher } from '~/assets/scripts/fetcher.js'

export const getLockCredits = async () => {
  const result = await fetcher('transaction/credits-lock', 'GET')
  return result
}

export const getWithdrawAccounts = async () => {
  const result = await fetcher('transaction/withdraw/accounts', 'GET')
  return result
}

export const addWithdrawAccounts = async (data) => {
  const result = await fetcher('transaction/withdraw/accounts/add', 'POST', {
    owner_name: data.accountName,
    bank_no: data.bankCode,
    bank_name: data.bankName,
    card_type: data.bankType,
    card_no: data.bankAccount,
    branch_name: '',
  })
  return result
}

export const removeWithdrawAccounts = async (data) => {
  const result = await fetcher('transaction/withdraw/accounts/remove', 'POST', {
    no: data.bankNumber,
  })
  return result
}

export const getWithdrawAccountsInfo = async () => {
  const result = await fetcher('transaction/withdraw/check', 'GET')
  return result
}

export const withdraw = async (data) => {
  const result = await fetcher('transaction/withdraw/apply', 'POST', {
    no: data.no,
    amount: data.amount,
  })
  return result
}

export const getDepositAccountsInfo = async () => {
  const result = await fetcher('transaction/deposit/check', 'GET')
  return result
}

export const deposit = async (data) => {
  const result = await fetcher('transaction/deposit/apply', 'POST', {
    amount: data.amount,
    pay_gateway: data.payGateway,
    user_bank_no: data.withdrawBanknNo,
    deposit_bank_no: data.depositBankNo,
  })
  return result
}

export const getDepositAccounts = async (data) => {
  const result = await fetcher('transaction/deposit/banks', 'POST', {
    pay_gateway: data.payGateway,
  })
  return result
}

export const getDepositType = async () => {
  const result = await fetcher('transaction/deposit/type', 'GET')
  return result
}

export const getVerificationPhone = async (data) => {
  const result = await fetcher('transaction/send-valid-code-phone', 'POST', {
    phone_no: data.phoneNumber,
    country_code: data.countryCode,
  })
  return result
}

export const getVerificationEmail = async (data) => {
  const result = await fetcher('transaction/send-valid-code-email', 'POST', {
    email: data.email,
  })
  return result
}

export const getBankCode = async () => {
  const result = await fetcher('transaction/bank-code', 'GET')
  return result
}

export default {
  getLockCredits,
  getWithdrawAccounts,
  addWithdrawAccounts,
  removeWithdrawAccounts,
  getWithdrawAccountsInfo,
  withdraw,
  getDepositAccountsInfo,
  deposit,
  getDepositAccounts,
  getDepositType,
  getVerificationPhone,
  getVerificationEmail,
  getBankCode,
}
