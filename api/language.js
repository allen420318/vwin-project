import { fetcher } from '~/assets/scripts/fetcher.js'

export const getLanguageList = async () => {
  const result = await fetcher('lang/list', 'GET')
  return result
}

export const getCurrentLanguage = async () => {
  const result = await fetcher('lang/current', 'GET')
  return result
}

export const setLanguage = async (data) => {
  const result = await fetcher('lang/set', 'POST', {
    lang: data.language,
  })
  return result
}

export default {
  getLanguageList,
  getCurrentLanguage,
  setLanguage,
}
