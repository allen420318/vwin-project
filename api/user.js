import { fetcher } from '~/assets/scripts/fetcher.js'

export const userLogin = async (data) => {
  const result = await fetcher('login', 'POST', {
    account: data.userAccount,
    login_password: data.userPassword,
    captcha: data.verificationImage,
    key: data.key,
  })
  return result
}

export const userRegister = async (data) => {
  const result = await fetcher('register', 'POST', {
    account: data.userAccount,
    login_password: data.userPassword,
    introducer_no: data.introducerNumber,
  })
  return result
}

export const getVerificationImage = async (type) => {
  const result = await fetcher(`get-captcha/${type}`, 'GET')
  return result
}

export const checkPhoneNumber = async (data) => {
  const result = await fetcher('check-phone ', 'POST', {
    account: data.userAccount,
    phone_no: data.phoneNumber,
    country_code: data.countryCode,
  })
  return result
}

export const getVerificationPhone = async (data) => {
  const result = await fetcher('send-valid-code-phone ', 'POST', {
    account: data.userAccount,
    phone_no: data.phoneNumber,
  })
  return result
}

export const checkEmail = async (data) => {
  const result = await fetcher('check-email  ', 'POST', {
    account: data.userAccount,
    email: data.email,
  })
  return result
}

export const getVerificationEmail = async (data) => {
  const result = await fetcher('send-valid-code-email ', 'POST', {
    account: data.userAccount,
    email: data.email,
  })
  return result
}

export const checkVerification = async (data) => {
  const result = await fetcher('check-valid-code', 'POST', {
    code: data.code,
  })
  return result
}

export const resetPassword = async (data) => {
  const result = await fetcher('rest-pwd', 'POST', {
    login_password: data.userPassword,
  })
  return result
}

export const getCountryCode = async () => {
  const result = await fetcher('get-country-code', 'GET')
  return result
}

export default {
  userLogin,
  userRegister,
  getVerificationImage,
  checkPhoneNumber,
  getVerificationPhone,
  checkEmail,
  getVerificationEmail,
  checkVerification,
  resetPassword,
  getCountryCode,
}
