import { fetcher } from '~/assets/scripts/fetcher.js'

export const getBettingOverview = async (data) => {
  const result = await fetcher('report/bet-all', 'POST', {
    start_time: data.startTime,
    end_time: data.endTime,
  })
  return result
}
export const getBettingRecord = async (data) => {
  const result = await fetcher('report/bet', 'POST', {
    provider: data.provider,
    provider_type: data.type,
    start_time: data.startTime,
    end_time: data.endTime,
    page: data.page,
    page_size: data.pageSize,
    state: data.state,
  })
  return result
}

export const getBettingTotal = async (data) => {
  const result = await fetcher('report/bet-sum ', 'POST', {
    provider: data.provider,
    provider_type: data.type,
    start_time: data.startTime,
    end_time: data.endTime,
    state: data.state,
  })
  return result
}

export const getDepositRecord = async (data) => {
  const result = await fetcher('report/deposit', 'POST', {
    start_time: data.startTime,
    end_time: data.endTime,
    page: data.page,
    page_size: data.pageSize,
  })
  return result
}

export const getDepositDetail = async (id) => {
  const result = await fetcher(`report/deposit/${id}`, 'GET')
  return result
}

export const getWithdrawRecord = async (data) => {
  const result = await fetcher('report/withdraw', 'POST', {
    start_time: data.startTime,
    end_time: data.endTime,
    page: data.page,
    page_size: data.pageSize,
  })
  return result
}

export const getWithdrawDetail = async (id) => {
  const result = await fetcher(`report/withdraw/${id}`, 'GET')
  return result
}

export const getTransferRecord = async (data) => {
  const result = await fetcher('report/transfer', 'POST', {
    start_time: data.startTime,
    end_time: data.endTime,
    page: data.page,
    page_size: data.pageSize,
  })
  return result
}

export const getTransferDetail = async (id) => {
  const result = await fetcher(`report/transfer/${id}`, 'GET')
  return result
}

export const getBonusRecord = async (data) => {
  const result = await fetcher('report/bonus', 'POST', {
    start_time: data.startTime,
    end_time: data.endTime,
    page: data.page,
    page_size: data.pageSize,
  })
  return result
}

export const getBonusDetail = async (id) => {
  const result = await fetcher(`report/bonus/${id}`, 'GET')
  return result
}

export default {
  getBettingOverview,
  getBettingRecord,
  getBettingTotal,
  getDepositRecord,
  getDepositDetail,
  getWithdrawRecord,
  getWithdrawDetail,
  getTransferRecord,
  getTransferDetail,
  getBonusRecord,
  getBonusDetail,
}
