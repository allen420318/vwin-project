import { fetcher } from '~/assets/scripts/fetcher.js'

export const getVIPInfo = async () => {
  const result = await fetcher('vip/level', 'GET')
  return result
}

export const getVIPLevel = async () => {
  const result = await fetcher('vip/levels-info', 'GET')
  return result
}

export default {
  getVIPInfo,
  getVIPLevel,
}
