import { fetcher } from '~/assets/scripts/fetcher.js'

export const getProviderCredits = async (data) => {
  const result = await fetcher('wallet/credits', 'POST', {
    provider: data.provider,
    type: data.type,
  })
  return result
}

export const transferInProvider = async (data) => {
  const result = await fetcher('wallet/transfer/in', 'POST', {
    provider: data.provider,
    type: data.type,
    amount: data.amount,
  })
  return result
}

export const transferOutProvider = async (data) => {
  const result = await fetcher('wallet/transfer/out', 'POST', {
    provider: data.provider,
    type: data.type,
    amount: data.amount,
  })
  return result
}

export const setAutoTransfer = async (data) => {
  const result = await fetcher('wallet/transfer/set-auto', 'POST', {
    auto_pay: data.autoPay,
  })
  return result
}

export default {
  getProviderCredits,
  transferInProvider,
  transferOutProvider,
  setAutoTransfer,
}
