# richbet-front

## Moblie

### Folder Structure

##### components

- header
  - index-header 首頁
  - page-header 頁面
  - member-header 會員
- navigation
  - index-bottom-nav 底部導覽列
- transferWindows 快速轉帳
- walletOverview 錢包總覽
- selectBank 請選擇銀行
- filterBar 過濾器
- reminderWindows 溫馨提示
- ImgSVG icon
- marquee 跑馬燈

##### pages

- index 首頁
- EGame
  - index 電子遊戲
  - Search 搜尋
- activity
  - index 優惠活動
  - activityDetail 活動內容
  - noviceTask 新手任務
  - vipDiscount VIP 優惠
  - inviteFriends 邀請好友
- MessageCenter
  - index 消息中心
  - MessageDetail 訊息詳情
- Login 登入
- ForgetPassword 忘記密碼
- Register 會員註冊
- CustomerService 我的客服
- PersonalCenter
  - index 我的
  - PersonalInfo 個人資料
  - PersonalVerification 個人資料驗證
  - VIPPrivilege VIP 特權
  - VIPDetail VIP 詳情
  - BettingRecord 投注記錄
  - TransactionRecord 交易記錄
  - RewardRecord 兌獎記錄
  - RecordDetail 交易詳情
- Wallet
  - index 我的錢包
  - Deposit 存款
  - DepositDetail 存款詳情
  - Withdrawal 提款
  - Transfer 轉帳
  - CardManage 卡片管理
  - AddBankAccount 新增銀行帳戶
- HelpCenter 幫助中心
- Cooperation 代理合作
- AboutUs 關於我們
- Setting
  - index 設定
  - ChangePassword 修改密碼
  - Language 語言
  - Feedback 意見反饋
  - MyFeedback 我的反饋
  - FeedbackDetail 反饋詳情

## PC

##### components

##### pages

### Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
