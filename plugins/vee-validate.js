import Vue from 'vue'
import { ValidationProvider, ValidationObserver, extend } from 'vee-validate'
import {
  required,
  min,
  max,
  regex,
  is,
  integer,
  min_value as minValue,
  max_value as maxValue,
} from 'vee-validate/dist/rules'

Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)

extend('required', required)
extend('regex', regex)
extend('max', max)
extend('min', min)
extend('is', is)
extend('integer', integer)
extend('minValue', minValue)
extend('maxValue', maxValue)
