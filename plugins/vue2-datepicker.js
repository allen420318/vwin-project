import Vue from 'vue'
import DatePicker from 'vue2-datepicker'
import 'vue2-datepicker/locale/zh-tw'
import 'vue2-datepicker/locale/zh-cn'
import 'vue2-datepicker/index.css'

Vue.component('date-picker', DatePicker)
