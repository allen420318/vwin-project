export default ({ app, store }) => {
  app.router.beforeEach((to, from, next) => {
    const isLogin = store.getters['login/getIsLogin']
    const routeTargetName = to.name
    const notCheckLoginRouteNames = [
      'index',
      'EGame',
      'EGame-Search',
      'PersonalCenter',
      'CustomerService',
      'Activity',
      'Activity-NoviceTask',
      'Activity-VIPDiscount',
      'Activity-InviteFriends',
      'Setting',
      'Setting-Language',
      'Sport',
      'Live',
      'Lotto',
      'Poker',
      'ESport',
    ]
    const loggedInRouteNames = ['Login', 'Register', 'ForgetPassword']
    if (isLogin) {
      if (
        loggedInRouteNames.includes(routeTargetName) === true &&
        notCheckLoginRouteNames.includes(routeTargetName) === false
      ) {
        next('/')
      }
      next()
    } else {
      if (
        loggedInRouteNames.includes(routeTargetName) === false &&
        notCheckLoginRouteNames.includes(routeTargetName) === false
      ) {
        next('/Login')
      }
      next()
    }
  })
}
