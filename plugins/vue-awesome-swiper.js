import Vue from 'vue'
import VueAwesomeSwiper from 'vue-awesome-swiper'

// import style
import 'swiper/swiper-bundle.css'

import {
  Swiper as SwiperClass,
  Navigation,
  Pagination,
  Autoplay,
  EffectFade,
} from 'swiper/swiper.esm'
import getAwesomeSwiper from 'vue-awesome-swiper/dist/exporter'
SwiperClass.use([Navigation, Pagination, Autoplay, EffectFade])
Vue.use(getAwesomeSwiper(SwiperClass))

Vue.use(VueAwesomeSwiper /* { default options with global component } */)
