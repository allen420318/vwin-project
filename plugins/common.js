import Vue from 'vue'
import { MessageBox, Message } from 'element-ui'

Vue.mixin({
  data: () => ({
    countdownStatus: false,
    countdownDefault: true,
    countdown: 0,
  }),
  methods: {
    showErrorMessage(error, callBack) {
      MessageBox.alert(error, {
        showClose: false,
        showCancelButton: false,
        confirmButtonText: '確定',
        closeOnPressEscape: false,
        center: true,
        callback: callBack,
      })
    },
    showErrorToast(msg) {
      Message.error({
        message: msg,
        center: true,
      })
    },
    showToast(msg) {
      Message({
        message: msg,
        center: true,
      })
    },
    copyText(targetId) {
      const node = this.$el.querySelector(targetId)
      const selection = window.getSelection()
      const range = document.createRange()
      range.selectNodeContents(node)
      selection.removeAllRanges()
      selection.addRange(range)
      document.execCommand('Copy')
      this.showToast(this.$t('depositDetail.copySuccess'))
    },
    countdownTimer(time) {
      this.countdownStatus = true
      if (this.countdownDefault) {
        this.countdown = time
        this.countdownDefault = false
      }
      if (this.countdown <= 0) {
        this.countdownStatus = false
        this.countdownDefault = true
        this.countdown = time
        return
      } else {
        setTimeout(this.countdownTimer, 1000)
      }
      this.countdown -= 1
    },
    formatNumber(num, digits = 2) {
      if (num === undefined) return '0.00'
      if (typeof num === 'string') return num
      let result = '1'
      for (let i = 1; i <= digits; i++) {
        result += '0'
      }
      const decimal = (
        Math.floor(parseInt(num) * parseInt(result)) / parseInt(result)
      ).toFixed(digits)
      const thousandth = decimal.toString().split('.')
      thousandth[0] = thousandth[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',')
      return thousandth.join('.')
    },
  },
})
